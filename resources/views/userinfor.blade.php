@extends('shared._layout')
@section('title','Home')
@section('custom-css')
@endsection
@section('content')
    <div class="container">
        <form action="{{ route('updateInfor') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="username">UserName</label>
                <input type="text" name="username" id="username" class="form-control" value="{{ $user -> username }}">
            </div>
            <div class="form-group">
                <label>
                    <input type="checkbox" id="changePassword" name="changePassword" style="display:block" value="true"> ChangePassword??
                </label>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" name="password" id="password" class="password form-control" disabled>
            </div>
            <div class="form-group">
                <label for="confirmPass">Confirm Password</label>
                <input type="password" name="confirmPass" id="confirmPass" class="password form-control" disabled>
            </div>
            <div class="form-group">
                <button class="btn btn-success">Save change</button>
            </div>
        </form>
    </div>

@endsection
@section('custom-script')
<script>
    var $jq = jQuery.noConflict()
    $jq(document).ready(function(){
        $jq("#changePassword").change(function(){
            if($jq(this).is(":checked"))
            {
                $jq(".password").removeAttr('disabled');
            }
            else
            {
                $jq(".password").attr('disabled','');
            }
        });
    });
</script>
@endsection