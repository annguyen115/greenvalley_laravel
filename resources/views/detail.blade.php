@extends('shared._layout')
@section('title','Product')
@section('custom-css')
	<style>
		.comment-box{
			border: 1px solid #dfe2e5;
			padding: 7px 10px;
			margin-bottom:20px;
		}
		.comment-username{
			color:#03658c;
			font-weight: 700;
			font-size: 14;
			margin-right: 10px;
		}
		.comment-date{
			color:#999;
			font-size: 13px;
		}
		.comment-box-header{
			padding-bottom: 10px;
			border-bottom: 1px solid #dfe2e5;
		}
		.comment-box-body{
			margin-top: 10px;
		}
	</style>
@endsection
@section('content')
	@php
		$id = $product->id;
	@endphp
    <div class="inner-header">
        <div class="container">
            <div class="pull-left">
                <h6 class="inner-title">Product</h6>
            </div>
            <div class="pull-right">
                <div class="beta-breadcrumb font-large">
                    <a href="{{ route('home',['name'=>'home']) }}">Home</a> / <span>Product</span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

	<div class="container">
		<div id="content">
			<div class="row">
				<div class="col-sm-9">

					<div class="row">
						<div class="col-sm-4" style="padding-left:30px;padding-right:0px">
							@if ($product->sale_price>0)
								<div class="ribbon-wrapper">
									<div class="ribbon sale">Sale</div>
								</div>
							@endif
							<img src="images/products/{{ $product->image }}" class="image-medium img-fluid thumbnail" alt="">
						</div>
						<div class="col-sm-8">
							<div class="single-item-body">
								<p class="single-item-title">{{ $product->name }}</p>
								<p class="single-item-price">
									@if ($product->sale_price>0)
										<span class="flash-del">${{ $product->price }}</span>
										<span class="flash-sale">${{ $product->sale_price }}</span>
									@else
										<span>${{ $product->price }}</span>
									@endif
								</p>
							</div>

							<div class="clearfix"></div>
							<div class="space20">&nbsp;</div>

							<div class="single-item-desc">
								<p>{{ $product->description }}</p>
							</div>
							<div class="space20">&nbsp;</div>

							<p>Options:</p>
							<div class="single-item-options">
								{{-- <select class="wc-select" name="size">
									<option>Size</option>
									<option value="XS">XS</option>
									<option value="S">S</option>
									<option value="M">M</option>
									<option value="L">L</option>
									<option value="XL">XL</option>
								</select>
								<select class="wc-select" name="color">
									<option>Color</option>
									<option value="Red">Red</option>
									<option value="Green">Green</option>
									<option value="Yellow">Yellow</option>
									<option value="Black">Black</option>
									<option value="White">White</option>
								</select> --}}
								<select class="wc-select" name="color">
									<option>Qty</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
								</select>
								<a class="add-to-cart" href="{{route('themgiohang',['id'=>$product->id])}}"><i class="fa fa-shopping-cart"></i></a>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>

					<div class="space40">&nbsp;</div>
					<div class="woocommerce-tabs">
						<ul class="tabs">
							<li><a href="#tab-description">Description</a></li>
						<li><a href="#tab-reviews">Reviews (<span id="comment-count"></span>)</a></li>
						</ul>

						<div class="panel" id="tab-description">
							<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet.</p>
							<p>Consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequaturuis autem vel eum iure reprehenderit qui in ea voluptate velit es quam nihil molestiae consequr, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? </p>
						</div>
						<div class="panel" id="tab-reviews">
							<div class="comment-list">

							</div>
							<div class="comment-new">
								@guest
									<span>Plese <a href="{{ route('login') }}" style="font-weight:700;color:03658c">Login</a> to review this product	</span>
								@else
									<form action="{{ route('insertComment') }}" method="POST">
										@csrf
										<input type="hidden" name="pid" value="{{ $id }}">
										<textarea name="comment" cols="10" rows="10" style="height:120px;margin-bottom:5px;"></textarea>
										<button class="btn btn-primary">Comment</button>
									</form>
								@endguest
							</div>
						</div>
					</div>
					<div class="space50">&nbsp;</div>
					<div class="beta-products-list">
						<h4>Related Products</h4>

						<div class="row">
							@foreach ($relateProducts as $product)
								<div class="col-sm-4">
									<div class="single-item">
										<div class="single-item-header">
											<a href="{{ route('ProductDetail')}}?id={{ $product->id }}"><img src="images/products/{{ $product->image }}" class="image-medium img-fluid thumbnail" alt=""></a>
										</div>
										<div class="single-item-body">
											<a href="{{ route('ProductDetail')}}?id={{ $product->id }}"><p class="single-item-title">{{ $product->name }}</p></a>
											<p class="single-item-price">
												<span>{{ $product->price }}</span>
											</p>
										</div>
										<div class="single-item-caption">
											<a class="add-to-cart pull-left" href="{{route('themgiohang',['id'=>$product->id])}}"><i class="fa fa-shopping-cart"></i></a>
											<a class="beta-btn primary" href="{{ route('ProductDetail')}}?id={{ $product->id }}">Details <i class="fa fa-chevron-right"></i></a>
											<div class="clearfix"></div>
										</div>
									</div>
								</div>
							@endforeach
							
						</div>
					</div> <!-- .beta-products-list -->
				</div>
				<div class="col-sm-3 aside">
					<div class="widget">
						<h3 class="widget-title">Best Sellers</h3>
						<div class="widget-body">
							<div class="beta-sales beta-lists">
								@foreach ($bestSellingProduct as $product)
									<div class="media beta-sales-item">
										<a class="pull-left" href="{{ route('ProductDetail')}}?id={{ $product->id }}"><img src="images/products/{{ $product->image }}" alt=""></a>
										<div class="media-body"><a href="{{ route('ProductDetail')}}?id={{ $product->id }}">{{ $product->name }}</a>
											<div>
												@if ($product->sale_price>0)
													<span class="beta-sales-price-sale">${{ $product->sale_price }}</span>
													<span class="beta-sales-price">${{ $product->price }}</span>
												
												@else
													<span class="beta-sales-price">${{ $product->price }}</span>
												@endif
											</div>
										</div>
									</div>
								@endforeach
							</div>
						</div>
					</div> <!-- best sellers widget -->
					<div class="widget">
						<h3 class="widget-title">New Products</h3>
						<div class="widget-body">
							<div class="beta-sales beta-lists">
								@foreach ($newProduct as $product)
									<div class="media beta-sales-item">
										<a class="pull-left" href="{{ route('ProductDetail')}}?id={{ $product->id }}"><img src="images/products/{{ $product->image }}" alt=""></a>
										<div class="media-body"><a href="{{ route('ProductDetail')}}?id={{ $product->id }}">{{ $product->name }}</a>
											<div>
												@if ($product->sale_price>0)
													<span class="beta-sales-price-sale">${{ $product->sale_price }}</span>
													<span class="beta-sales-price">${{ $product->price }}</span>
												
												@else
													<span class="beta-sales-price">${{ $product->price }}</span>
												@endif
											</div>
										</div>
									</div>	
								@endforeach
							</div>
						</div>
					</div> <!-- best sellers widget -->
				</div>
			</div>
		</div> <!-- #content -->
	</div> <!-- .container -->

@endsection
@section('custom-script')
	<script>
		$(document).ready(function($){
			getComment($);
		});
		function getComment($){
			$.ajax({
				type:'GET',
				dataType: 'html',
				url:'{{ route('getComments',['id'=>$id]) }}',
				success:function(response) {
					console.log(response);
					response = JSON.parse(response);
					$('.comment-list').html(response.view);
					$('#comment-count').text(response.count)
				}
			});
		}
	</script>
@endsection