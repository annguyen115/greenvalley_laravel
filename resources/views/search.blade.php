@extends('shared._layout')
@section('title','Search')
@section('custom-css')
@endsection
@section('content')
<div class="space60">&nbsp;</div>
<div class="container">
    <div class="wrapper">
        <div class="row" style="margin:auto">
        <form class="form-horizontal" role="form" method="GET" action="{{url('advanceSearch')}}">
                @csrf
                <select id="catID" style="width:200px;height:35px" name="catID">
                    <option value="">Chọn Chủ Đề</option>
                    @foreach (App\Category::all() as $cat)
                    <option value="{{$cat->id}}">{{$cat->name}}</option>
                    @endforeach
                </select> 
                <select id="typeID" style="width:200px;height:35px" name="typeID">
                    <option value="">Chọn Loại</option>
                    @foreach (App\Type::all() as $type)
                    <option value="{{$type->id}}">Hoa {{$type->name}}</option>
                    @endforeach
                </select> 
                <select id="priceID" style="width:200px;height:35px">
                    <option value="">Chọn Gia</option>
                    <option value="0-100">0-100</option>
                    <option value="100-300">100-300</option>
                    <option value="300-500">300-500</option>
                    <option value="500-1000">500-1000</option>
                    <option value="1000-2000">1000-2000</option>
                    <option value="2000-50000">2000-50000</option>
                </select>
                <button class="btn btn-success" type="submit" id="searchBtn">Tìm</button>
            </form>
        </div>
        <h4>Tìm Kiếm</h4>
        <div class="beta-products-details">
            <p class="pull-left">Tìm thấy {{count($productList)}} sản phẩm</p>
            <div class="clearfix"></div>
        </div>
        <div class="row">
            @foreach ($productList as $product)
                <div class="col-sm-3">
                    <div class="single-item" style="margin-bottom:20px">
                        @if ($product->sale_price>0)
                            <div class="ribbon-wrapper">
                                <div class="ribbon sale">Sale</div>
                            </div>
                        @endif
                        <div class="single-item-header">
                            <a href="{{ route('ProductDetail')}}?id={{ $product->id }}"><img src="images/products/{{ $product->image }}" class="image-medium" alt=""></a>
                        </div>
                        <div class="single-item-body">
                            <p class="single-item-title"><a href="{{ route('ProductDetail')}}?id={{ $product->id }}">{{ $product->name }}</a></p>
                            <p class="single-item-price">
                                @if ($product->sale_price>0)
                                    <span class="flash-del">${{ $product->price }}</span>
                                    <span class="flash-sale">${{ $product->sale_price }}</span>
                                @else
                                    <span>${{ $product->price }}</span>
                                @endif
                            </p>
                        </div>
                        <div class="single-item-caption">
                            <a class="add-to-cart pull-left" href="{{route('themgiohang',['id'=>$product->id])}}"><i class="fa fa-shopping-cart"></i></a>
                            <a class="beta-btn primary" href="{{ route('ProductDetail')}}?id={{ $product->id }}">Details <i class="fa fa-chevron-right"></i></a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            @endforeach          
        </div>
    </div>
    <div class="space40">&nbsp;</div>
</div> 
@endsection
@section('custom-script')
@endsection