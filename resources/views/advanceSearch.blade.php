@extends('shared._layout')
@section('title','Search')
@section('custom-css')
@endsection
@section('content')
<div class="container">
<div class="main-content">
    <div class="wrapper">
        <div class="row">
            <div class="col-sm-12">
            <div class="breadcrumbs">
                <ul>
                    <li style="list-style: none; display:inline"><a href="#">Home </a></li>
                    <li style="list-style: none; display:inline"><span class="dot">/</span>
                @if(count($data)=="0")
                <b>{{$catByUser}}</b>
                @else
                @if($catByUser=="All Products")
                <a href="{{url('advanceSearch')}}">
                {{$catByUser}}</a>
                @else
                <a href="{{url('advanceSearch')}}/{{$catByUser}}">
                    {{$catByUser}}</a>
                    @endif
                @endif
                </li>
                </ul>
            </div>
            </div>
        </div>

    @if(count($data)!="0")
    <h1 class="text-center">{{$catByUser}} </h1>
    <div class="space60">&nbsp;</div>
    <div class="row" style="margin:auto">
        {{-- <form class="form-horizontal" role="form" method="" action=""> --}}
            {{ csrf_field() }}
            {{-- chon chu de --}}
            <select id="catID" style="width:200px;height:35px" name="catID">
                <option value="">Chọn Chủ Đề</option>
                @foreach (App\Category::all() as $cat)
            <option value="{{$cat->id}}">{{$cat->name}}</option>
                @endforeach
            </select> 
            {{-- chon loai hoa --}}
            <select id="typeID" style="width:200px;height:35px" >
                <option value="">Chọn  Loại</option>
                @foreach (App\Type::all() as $type)
            <option id="type{{$type->id}}" value="{{$type->id}}">Hoa {{$type->name}}</option>
                @endforeach
            </select> 
            {{-- chon gia --}}
            <select id="priceID" style="width:200px;height:35px">
                <option value="">Chọn Gia</option>
                <option value="0-100">0-100</option>
                <option value="100-300">100-300</option>
                <option value="300-500">300-500</option>
                <option value="500-1000">500-1000</option>
                <option value="1000-2000">1000-2000</option>
                <option value="2000-50000">2000-50000</option>
            </select>
            <button class="btn btn-success" type="submit" id="searchBtn">Tìm</button>
        {{-- </form> --}}
        </div>
        @endif
        <div class="space40">&nbsp;</div>
        <div class="row">
            @if(count($data)=="0")
            <div class="col-md-12" align="center">
                <h1 align="center" style="margin:20px">
                    Không tìm thấy sản phẩm trong <b style="color:red">{{$catByUser}} !</b></h1>
            </div>
            @else

            {{-- lay tu ajax outJS --}}
            <div id="productData">
            @foreach ($data as $p)
                <div class="col-sm-3">
                    <div class="single-item">
                        @if ($p->sale_price>0)
                            <div class="ribbon-wrapper">
                                <div class="ribbon sale">Sale</div>
                            </div>
                        @endif
                        <div class="single-item-header">
                            <a href="{{ route('ProductDetail')}}?id={{ $p->id }}"><img src="images/products/{{ $p->image }}" class="image-medium" alt=""></a>
                        </div>
                        <div class="single-item-body">
                            <p class="single-item-title"><a href="{{ route('ProductDetail')}}?id={{ $p->id }}">{{ $p->name }}</a></p>
                            <p class="single-item-price">
                                @if ($p->sale_price>0)
                                    <span class="flash-del">${{ $p->price }}</span>
                                    <span class="flash-sale">${{ $p->sale_price }}</span>
                                @else
                                    <span>${{ $p->price }}</span>
                                @endif
                            </p>
                        </div>
                        <div class="single-item-caption">
                            <a class="add-to-cart pull-left" href="shopping_cart.html"><i class="fa fa-shopping-cart"></i></a>
                            <a class="beta-btn primary" href="product.html">Details <i class="fa fa-chevron-right"></i></a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="space40">&nbsp;</div>
                </div>
            @endforeach         
            </div> {{-- end of productData --}}
        @endif
        </div>
        <div class="space40">&nbsp;</div>
    </div> <!-- end section with sidebar and main content -->
</div>
@endsection
@section('custom-script')
<script>
    
    $(document).ready(function($){
        $("#searchBtn").click(function(){
            var type= $("#typeID").val();
            var price= $("#priceID").val();
          //  var typeName=$("typeName").name();
            //var cat= $("#catID").val();
            // var data='type_id=' + type;
            // if(price=''){
            //     data+='&price=' + price;
            // }
            $.ajax({
                type: 'get',
                dataType: 'html',
                url: '{{url('/sortSearch')}}',
                data: 'type_id='+ type + '&price='+ price ,
                success:function(response){
                    console.log(response);
                    $("#productData").html(response);
                }
            });

        });
    
    });
</script>
@endsection