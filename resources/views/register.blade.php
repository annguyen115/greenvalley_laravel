@extends('shared._layout')
@section('title','Register')
@section('custom-css')
    <style>
        * {box-sizing: border-box}

        /* Add padding to containers */
        .container-form {
            margin: 0 auto;
            width: 500px;
            box-shadow: 0 0 0 10px rgba(0,0,0,0.1);
            padding: 20px;
            background: white;
            border: 2px solid black;
            border-radius: 10px;
        
        }

        .top-space{
            padding: 50px;
            /* background: #4AC29A;  
            background: -webkit-linear-gradient(to top, #BDFFF3, #4AC29A);  
            background: linear-gradient(to top, #BDFFF3, #4AC29A);  */
            background: #A1FFCE;  
            background: -webkit-linear-gradient(to bottom, #FAFFD1, #A1FFCE); 
            background: linear-gradient(to bottom, #FAFFD1, #A1FFCE);



        }
        /* Full-width input fields */
        input[type=text], input[type=password],input[type=date],input[type=tel] {
        width: 100%;
        padding: 15px;
        margin: 5px 0 22px 0;
        display: inline-block;
        border: none;
        background: #f1f1f1;
        }

        input[type=text]:focus, input[type=password]:focus,input[type=date]:focus,input[type=tel]:focus{
        background-color: #ddd;
        outline: none;
        }

        /* Overwrite default styles of hr */
        hr {
        border: 1px solid #f1f1f1;
        margin-bottom: 25px;
        }

        /* Set a style for the submit/register button */
        .registerbtn {
        background-color: #4CAF50;
        color: white;
        padding: 16px 20px;
        margin: 8px 0;
        border: none;
        cursor: pointer;
        width: 100%;
        opacity: 0.9;
        }

        .registerbtn:hover {
        opacity:1;
        }

        /* Add a blue text color to links */
        a {
        color: dodgerblue;
        }
        p,h1{
            text-align: center;
        }
        /* Set a grey background color and center the text of the "sign in" section */
        .signin {
        background-color: #f1f1f1;
        text-align: center;
        }
    </style>
@endsection
@section('content')
<div class="top-space">
    <form action="xyly.php" method="POST">
    <div class="container-form">
      <h1>Register</h1>
      <hr>
      <label for="username"><b>Username</b></label>
      <input type="text" placeholder="Enter Username" name="username" required>

      <label for="address"><b>Địa chỉ</b></label>
      <input type="text" placeholder="Enter Address" name="address" required>
      
      <label for="dob"><b>Ngày sinh</b></label>
      <input type="date" placeholder="Enter Date Of Birth" name="dob" required>
      
      <label for="phone"><b>Phone</b></label>
      <input type="tel" placeholder="Enter Phone" name="phone" required>
      
      <label for="email"><b>Email</b></label>
      <input type="text" placeholder="Enter Email" name="email" required>
  
      <label for="psw"><b>Password</b></label>
      <input type="password" placeholder="Enter Password" name="psw" required>

      <label for="psw-repeat"><b>Repeat Password</b></label>
      <input type="password" placeholder="Confirm Password" name="psw-repeat" required>


      <hr>

      <p>Please fill in this form to create an account.</p>
      <button type="submit" class="registerbtn" name="register">Register</button>
      <p>Already have an account? <a href="#">Sign in</a>.</p>
    </div>
  </form>
</div>

@endsection
@section('custom-script')
@endsection