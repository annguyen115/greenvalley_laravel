@extends('shared._layout')
@section('title','Shopping Cart')
@section('custom-css')
@endsection
@section('content')
    <div class="inner-header">
		<div class="container">
			<div class="pull-left">
				<h6 class="inner-title"><strong>Giỏ Hàng</strong></h6>
			</div>
			<div class="pull-right">
				<div class="beta-breadcrumb font-large">
					<a href="{{ route('home',['name'=>'home']) }}">Trang Chủ</a> / <span>Giỏ Hàng</span>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="container">
		<div id="content">
			<div class="table-responsive">
				@if(Session::has('cart')) {{-- begin if --}}
				<!-- Shop Products Table -->
					<table class="shop_table beta-shopping-cart-table" cellspacing="0">
						<thead>{{-- đầu --}}
							<tr>
								<th class="product-name">Sản Phẩm</th>
								<th class="product-price">Giá</th>
								<th class="product-quantity">Số Lượng</th>
								<th class="product-subtotal">Tổng tiền</th>
								<th class="product-remove">Xóa</th>
							</tr>
						</thead>
						
						@foreach ($product_cart as $product)
							<tbody>{{-- thân --}}
								<tr class="cart_item">
									{{-- cột tên product --}}
									<td class="product-name">
										<div class="media">
											<img class="pull-left" src="images/products/{{$product['item']['image']}}" style="width: 100px;height: 100px">
											<div class="media-body">
												<p class="font-large table-title">{{$product['item']['name']}}</p>
											</div>
										</div>
									</td>
									{{-- cột giá --}}
									<td class="product-price">
										<span class="amount">{{$product['item']['price']}}</span> </span>
									</td>
									{{-- cột số lượng sp --}}
									<td class="product-quantity">
									<span class="badge">{{$product['qty']}}</span>
									</td>
									{{-- cột giá tổng cộng của 1 sản phẩm --}}
									<td class="product-subtotal">
									<span class="amount"><span class="cart-total-value"><span class="cart-item-amount">{{$product['price']}}</span></span></span>
									</td>
									{{-- cột xóa sp --}}
									<td class="product-remove">
										<a href="{{route('xoagiohang',$product['item']['id'])}}" class="remove" title="Remove this item">
											<button type="button" class="btn btn-default btn-sm">
												<span class="glyphicon glyphicon-remove"></span> 
												Xóa 
											</button>
										</a>
									</td>
								</tr>
						@endforeach
						</tbody>
					</table><!-- End of Shop Table Products -->
					<!-- Cart Collaterals -->
					<div class="cart-collaterals">
						
						<div class="col-sm-6 col-md-6 col-md-offset-10 col-sm-offset-3">
								@if(Auth::user())
								
									<a href="{{route('dathang')}}" class="beta-btn primary text-center">Đặt hàng <i class="fa fa-chevron-right"></i></a>
							
								@else
									<a href="{{route('login')}}" class="beta-btn primary text-center">Đặt hàng <i class="fa fa-chevron-right"></i></a>
								@endif
								
								
						</div>
					

						<div class="cart-totals pull-right">
							<div class="cart-totals-row"><h5 class="cart-total-title">Tổng Tiền: @if(Session::has('cart')){{number_format($totalPrice)}}@else 0 @endif đồng</h5></div>
							
							<div class="cart-totals-row"><span>Vận Chuyển:</span> <span>Miễn Phí</span></div>
							<div class="cart-totals-row"><span>Tổng Cộng:</span> <span> @if(Session::has('cart')){{number_format($totalPrice)}}@else 0 @endif đồng</span></div>
						</div>

						<div class="clearfix"></div>
					</div>
			<!-- End of Cart Collaterals -->
					@else
						<div class="col-md-12" style="align: center">
							<h1  style="margin:20px; align:center">
								Không có sản phẩm nào trong giỏ hàng!
							</h1>
							<div class="pull-right">
								<a href="{{route('home',['name'=>'home'])}}" class="beta-btn primary text-center">Tiếp tục mua sắm<i class="fa fa-chevron-right"></i></a>

							</div>
						</div>
						</div>
				@endif{{-- end if --}}

			</div>

			
			<div class="clearfix"></div>

		</div> <!-- #content -->
	</div> <!-- .container -->

@endsection
@section('custom-script')
@endsection