@foreach ($data as $p)
<div class="col-sm-3">
    <div class="single-item">
        @if ($p->sale_price>0)
            <div class="ribbon-wrapper">
                <div class="ribbon sale">Sale</div>
            </div>
        @endif
        <div class="single-item-header">
            <a href="{{ route('ProductDetail')}}?id={{ $p->id }}"><img src="images/products/{{ $p->image }}" class="image-medium" alt=""></a>
        </div>
        <div class="single-item-body">
            <p class="single-item-title"><a href="{{ route('ProductDetail')}}?id={{ $p->id }}">{{ $p->name }}</a></p>
            <p class="single-item-price">
                @if ($p->sale_price>0)
                    <span class="flash-del">${{ $p->price }}</span>
                    <span class="flash-sale">${{ $p->sale_price }}</span>
                @else
                    <span>${{ $p->price }}</span>
                @endif
            </p>
        </div>
        <div class="single-item-caption">
            <a class="add-to-cart pull-left" href="shopping_cart.html"><i class="fa fa-shopping-cart"></i></a>
            <a class="beta-btn primary" href="product.html">Details <i class="fa fa-chevron-right"></i></a>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="space40">&nbsp;</div>
</div>
@endforeach         