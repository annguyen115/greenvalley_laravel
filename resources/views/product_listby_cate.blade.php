@extends('shared._layout')
@section('title','ListByCategory')
@section('custom-css')
	<style>
		.panel{
			border-radius: 3px ; 
			-webkit-border-radius: 3px ; 
			-moz-border-radius: 3px ; 
			-ms-border-radius: 3px ; 
			-o-border-radius: 3px ; 
		}
		.panel-default>.panel-heading{
			background: #008d47f0;
			padding: 10px 15px;
			font-weight: 500;
			color: #ffff;
			font-size: 14px;
		}
		.panel>.panel-body{
			padding: 0;
		}
		.list-group-item.list-group-item-action.active{
			background-color: inherit;
			border-color: #ddd;
			color:#2196F3;
		}
		#type-sort{
			display: inline;
			width: auto;
			float: right;
			height:30px;
			padding: 0 5px;
		}
		.select-sort:after{
			clear: both;
			display:table;
			content: '';
		}
	</style>
@endsection
@section('content')
	@php
		$category_id = $category->id;
		$page = isset($_GET['page']) ? $_GET['page'] : 1;
	@endphp
    <div class="inner-header">
		<div class="container">
			<div class="pull-left">
				<h6 class="inner-title">{{$category->name}}</h6>
			</div>
			<div class="pull-right">
				<div class="beta-breadcrumb font-large">
					<a href="{{ route('home',['name'=>'home']) }}">Home</a> / <span>{{$category->name}}</span>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="container">
		<div id="content" class="space-top-none">
			<div class="main-content">
				<div class="space60">&nbsp;</div>
				<div class="row">
					<div class="col-sm-3">
						{{-- <a href="#" class="list-group-item list-group-item-action active">
							Cras justo odio
						</a> --}}
						<div class="panel panel-default panel-category">
							<div class="panel-heading"><i class="fas fa-cubes"></i> Category List</div>
							<div class="panel-body list-group">
							@foreach ($listCategory as $category)
								<a href="{{ route('listbycategory') }}?category_id={{ $category->id }}" class="list-group-item list-group-item-action @if ($category->id==$category_id) {{ 'active' }} @endif">{{$category->name}}</a>
							@endforeach
							</div>
						</div>
						<div class="panel panel-default panel-category">
							<div class="panel-heading"><i class="fas fa-leaf"></i> Type List</div>
							<div class="panel-body list-group">
							@foreach ($listType as $type)
								<a href="{{ route('listbytype') }}?type_id={{ $type->id }}" class="list-group-item list-group-item-action">{{$type->name}}</a>
							@endforeach
							</div>
						</div>
					</div>
					<div class="col-sm-9">
						<div class="beta-products-list">
							<h2>Products List</h2>
							{{-- <div class="beta-products-details">
								<p class="pull-left"></p>
								<div class="clearfix"></div>
							</div> --}}
							@if (count($listProduct)==0)
								<div> No product was found</div>
							@else
								<div class="select-sort">
									<select id="type-sort">
										<option selected disabled>Lọc theo</option>
										@foreach ($listType as $type)
											<option value="{{ $type->id }}">{{ $type->name }}</option>
										@endforeach
										<option value="-1">Tất cả</option>
									</select>
								</div>
								<div class="space30">&nbsp;</div>
								<div class="row display-flex" id="product-list">
									@foreach ($listProduct as $product)
										<div class="col-sm-4" style="margin-bottom:20px">
											<div class="single-item">
												@if ($product->sale_price>0)
													<div class="ribbon-wrapper">
														<div class="ribbon sale">Sale</div>
													</div>
												@endif
												<div class="single-item-header">
														<a href="{{ route('ProductDetail')}}?id={{ $product->id }}"><img src="images/products/{{ $product->image }}" class="image-medium img-fluid thumbnail" alt=""></a>
												</div>
												<div class="single-item-body">
													<p class="single-item-title"><a href="{{ route('ProductDetail')}}?id={{ $product->id }}">{{ $product->name }}</a></p>
													<p class="single-item-price">
														@if ($product->sale_price>0)
															<span class="flash-del">${{ $product->price }}</span>
															<span class="flash-sale">${{ $product->sale_price }}</span>
														@else
															<span>${{ $product->price }}</span>
														@endif
													</p>
												</div>
												<div class="single-item-caption">
													<a class="add-to-cart pull-left" href="{{route('themgiohang',['id'=>$product->id])}}"><i class="fa fa-shopping-cart"></i></a>
													<a class="beta-btn primary" href="{{ route('ProductDetail')}}?id={{ $product->id }}">Details <i class="fa fa-chevron-right"></i></a>
													<div class="clearfix"></div>
												</div>
											</div>
										</div>
									@endforeach
								</div>
								@if ($pageTotal>1)
									<nav aria-label="Page navigation" id="pagging">
										<ul class="pagination">
											@if ($page>1)
												<li class="page-item"><a class="page-link" href="{{route('listbycategory').'?category_id='. $category_id.'&page='.($page-1)}}">Previous</a></li>
											@endif
											@for ($i = 1; $i <= $pageTotal; $i++)
												@php
													$href = route('listbycategory').'?category_id='. $category_id.'&page='.$i;
												@endphp
												<li class="page-item @if ($page==$i) {{ 'active' }} @endif"><a class="page-link" href="{{$href}}">{{ $i }}</a></li>
											@endfor
											@if ($page<$pageTotal)
												<li class="page-item"><a class="page-link" href="{{route('listbycategory').'?category_id='. $category_id.'&page='.($page+1)}}">Next</a></li>
											@endif
										</ul>
									</nav>
								@endif
							@endif
						</div> <!-- .beta-products-list -->							
					</div>
				</div> <!-- end section with sidebar and main content -->


			</div> <!-- .main-content -->
		</div> <!-- #content -->
	</div> <!-- .container -->
@endsection
@section('custom-script')
	<script>
		$(document).ready(function($){
			$('#type-sort').on('change',function(){
				$typeId = $(this).val();
				$cateId= {{ $category_id }};
				$.ajax({
					type: 'get',
					dataType: 'html',
					url: '{{ route("sortbytype") }}',
					data: 'category_id='+ $cateId + '&type_id='+ $typeId,
					success:function(response){
						console.log(response);
						response = JSON.parse(response);
						console.log(response.view);
						$('#product-list').html(response.view);
						$('#pagging').remove();
						// $("#productData").html(response);
					}
				});
			})
		})
	</script>
@endsection