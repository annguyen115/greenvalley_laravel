@foreach ($comments as $comment)
    <section class="comment-box">
        <div class="comment-box-header">
            <a href="#" class="comment-username">{{ $comment->user->username }}</a><span class="comment-date">{{ $comment->created_at }}</span>
        </div>
        <div class="comment-box-body">
           {{ $comment->content }}
        </div>
    </section>
@endforeach