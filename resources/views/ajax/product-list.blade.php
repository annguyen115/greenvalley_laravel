@if (count($listProduct)==0)
    <div> No product was found</div>
@else
    @foreach ($listProduct as $product)
    <div class="col-sm-4" style="margin-bottom:20px">
        <div class="single-item">
            @if ($product->sale_price>0)
                <div class="ribbon-wrapper">
                    <div class="ribbon sale">Sale</div>
                </div>
            @endif
            <div class="single-item-header">
                    <a href="{{ route('ProductDetail')}}?id={{ $product->id }}"><img src="images/products/{{ $product->image }}" class="image-medium img-fluid thumbnail" alt=""></a>
            </div>
            <div class="single-item-body">
                <p class="single-item-title"><a href="{{ route('ProductDetail')}}?id={{ $product->id }}">{{ $product->name }}</a></p>
                <p class="single-item-price">
                    @if ($product->sale_price>0)
                        <span class="flash-del">${{ $product->price }}</span>
                        <span class="flash-sale">${{ $product->sale_price }}</span>
                    @else
                        <span>${{ $product->price }}</span>
                    @endif
                </p>
            </div>
            <div class="single-item-caption">
                <a class="add-to-cart pull-left" href="shopping_cart.html"><i class="fa fa-shopping-cart"></i></a>
                <a class="beta-btn primary" href="product.html">Details <i class="fa fa-chevron-right"></i></a>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    @endforeach
@endif
