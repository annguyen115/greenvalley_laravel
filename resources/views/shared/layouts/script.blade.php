

    <script src="libs/jquery/dist/jquery.min.js"></script>
    <script src="libs/jqueryui/jquery-ui-1.10.4.custom.min.js"></script>
    <script src="libs/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="libs/bxslider/jquery.bxslider.min.js"></script>
	<script src="libs/colorbox/jquery.colorbox-min.js"></script>
	<script src="libs/animo/Animo.js"></script>
	<script src="libs/dug/dug.js"></script>
	<script src="build/js/scripts.min.js"></script>
	<script src="build/js/jquery.themepunch.tools.min.js"></script>
	<script src="build/js/jquery.themepunch.revolution.min.js"></script>
	<script src="build/js/waypoints.min.js"></script>
	<script src="build/js/wow.min.js"></script>
	<!--customjs-->
	<script src="build/js/custom2.js"></script>
	<script>
		$(document).ready(function ($) {
			$(window).scroll(function () {
				if ($(this).scrollTop() > 150) {
					$(".header-bottom").addClass('fixNav')
				} else {
					$(".header-bottom").removeClass('fixNav')
				}
			}
			)
		})
	</script>
	<script type="text/javascript">
		$(function ($) {
			// this will get the full URL at the address bar
			var url = window.location.href;

			// passes on every "a" tag
			$(".main-menu a").each(function () {
				// checks if its the same on the address bar
				if (url == (this.href)) {
					$(this).closest("li").addClass("active");
					$(this).parents('li').addClass('parent-active');
				}
			});
		});


	</script>