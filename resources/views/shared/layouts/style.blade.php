    <link href='http://fonts.googleapis.com/css?family=Dosis:300,400' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
	<!-- Bootstrap -->
    <link href="libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!--Fontawesom-->
	<link rel="stylesheet" href="libs/font-awesome-4/css/font-awesome.min.css">
	<link rel="stylesheet" href="libs/font-awesome-5/css/all.css">
    <link rel="stylesheet" href="libs/colorbox/example3/colorbox.css">
	<link rel="stylesheet" href="build/css/settings.css">
	<link rel="stylesheet" href="build/css/responsive.css">
	<link rel="stylesheet" title="style" href="build/css/style.css">
	<link rel="stylesheet" href="build/css/animate.css">
	<link rel="stylesheet" title="style" href="build/css/huong-style.css">
	<link rel="stylesheet" href="build/css/fixHeightBoostrap.css">
	{{-- fix chiều cao ảnh --}}
	<style>
		.image-medium{
			height: 250px;
			object-fit: cover;
		}
		.thumbnail{
			margin-bottom: 0;
		}
	</style>
	<style>
		.main-menu ul.l-inline>li>a:hover{
			background: #31b33c;
		}
		.main-menu ul.l-inline>li.parent-active>a{
			background: #31b33c;
		}
		.sub-menu li:hover, .children li:hover {
			background: #31b33c;
		}
		.main-menu ul li.active>a,.sub-menu li.parent-active, .children li.parent-active{
			background: #31b33c;
		}
	</style>