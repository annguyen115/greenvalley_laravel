<div id="header">
    <div class="header-top">
        <div class="container">
            <div class="pull-left auto-width-left">
                <ul class="top-menu menu-beta l-inline">
                    <li><a href=""><i class="fa fa-home"></i> Nguyễn Khắc Nhu, Bến Thành, Quận 1</a></li>
                    <li><a href=""><i class="fa fa-phone"></i> 090 222 8888</a></li>
                </ul>
            </div>
            <div class="pull-right auto-width-right">
                <ul class="top-details menu-beta l-inline">
                    @guest
                    <li><a href="#">Đăng kí</a></li>
                    <li><a href="{{ route('login') }}">Đăng nhập</a></li>
                    @else
                        <li><a href="{{ route('userinfor') }}"><i class="fa fa-user"></i> {{ Auth::user()->username }}</a></li>
                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> Thoát</a></li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    @endguest
                </ul>
            </div>
            <div class="clearfix"></div>
        </div> <!-- .container -->
    </div> 
        <!-- .header-top -->
        <div class="header-body">
            <div class="container beta-relative">
                <div class="pull-left">
                    <a href="{{ route('home',['name' => 'home']) }}" id="logo"><img src="images/logo3.png" width="200px" alt=""></a>
                </div>
                <div class="pull-right beta-components space-left ov">
                    <div class="space10">&nbsp;</div>
                    <div class="beta-comp">
                        <form role="search" method="get" id="searchform" action="{{ route('search') }}">
                            <input type="text" value="" name="key" id="s" placeholder="Nhập từ khóa..." />
                            <button class="fa fa-search" type="submit" id="searchsubmit"></button>
                        </form>
                    </div>
    
                    <div class="beta-comp">
                        <div class="cart">
                            <div class="">
                                {{-- chuyển tới giỏ hang page shopping_cart--}}
                                
                            <a href="{{ route('shoppingCart') }}" style="margin-right: 3px;color: #999da0;padding-left: 0;padding-right: 0;font-size: 16px;">
                                <i class="fa fa-shopping-cart" style="color: #999da0;"></i> 
                                Giỏ hàng 
                                <span class="badge">
                                    @if(Session::has('cart')){{Session('cart')->totalQty}}
                                    @else Trống
                                    @endif
                                </span>
                            </a>
                                 
                               
    
    
                                {{-- <i class="fa fa-chevron-down"></i> --}}
                            {{-- </div> --}}
                            {{-- <div class="beta-dropdown cart-body" style="width:400px"> --}}
                               
                                {{-- @if(Session::has('cart'))
                                @foreach(session::get('cart') as $product)
                                    <div class="cart-item">
                                        <a class="cart-item-delete" href="{{route('xoagiohang',$product['item']['id'])}}"><i class="fa fa-times"></i></a>
                                        <div class="media">
                                            <a class="pull-left" href="#"><img src="images/product/{{$product['item']['image']}}" alt=""></a>
                                            <div class="media-body">
                                                <span class="cart-item-title">{{$product['item']['name']}}</span>
                                                <span class="cart-item-amount">{{$product['qty']}}*<span>@if($product['item']['sale_price']==0){{number_format($product['items']['price'])}} @else {{number_format($product['items']['sale_price'])}}@endif</span></span>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                    <div class="cart-caption">
                                        <div class="cart-total text-right">Tổng tiền: <span class="cart-total-value">@if(Session::has('cart')){{number_format($totalPrice)}} @else 0 @endif đồng</span></div>
                                        <div class="clearfix"></div>
    
                                        <div class="center">
                                            <div class="space10">&nbsp;</div>
                                            <a href="{{route('dathang')}}" class="beta-btn primary text-center">Đặt hàng <i class="fa fa-chevron-right"></i></a>
                                        </div>
                                    </div>
    
                                @endif --}}
                            </div>
                        </div> <!-- .cart -->
                    </div>
                </div>
                <div class="clearfix"></div>
            </div> <!-- .container -->
        </div> <!-- .header-body -->
        <div class="header-bottom" style="background-color: #008d47;">
            <div class="container">
                <a class="visible-xs beta-menu-toggle pull-right" href="#"><span class='beta-menu-toggle-text'>Menu</span> <i class="fa fa-bars"></i></a>
                <div class="visible-xs clearfix"></div>
                <nav class="main-menu">
                    <ul class="l-inline ov">
                        <li><a href="{{ route('home',['name'=>'home']) }}">Trang chủ</a></li>
                        <li><a href="{{ route('listall') }}">Sản phẩm</a>
                            <ul class="sub-menu">
                                @foreach ($listCategory as $category)
                                    <li><a href="{{ route('listbycategory') }}?category_id={{ $category->id }}">{{ $category->name }}</a></li>
                                @endforeach
                            </ul>
                        </li>
                        <li><a href="{{ route('about') }}">Giới thiệu</a></li>
                        <li><a href="{{ route('contact') }}">Liên hệ</a></li>
                    </ul>
                    <div class="clearfix"></div>
                </nav>
            </div> <!-- .container -->
        </div> <!-- .header-bottom -->
    </div>