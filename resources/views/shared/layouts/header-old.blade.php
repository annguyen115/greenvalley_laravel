
<div class="header-area">
    <div class="header_menu text-center" data-spy="affix" data-offset-top="50" id="nav">
        <div class="container">
            <nav class="navbar navbar-default zero_mp ">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
                        aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand custom_navbar-brand" href="#"><img src="images/logo.png" alt=""></a>
                </div>
                <!--End of navbar-header-->
    
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse zero_mp" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right main_menu">
                        <li class="active"><a href="{{ route('home',['name'=>'home'])}}">Home <span class="sr-only">(current)</span></a></li>
                        <li><a href="aboutus.php">about Us</a></li>
                        <li><a href="{{ route('listproduct') }}">Store</a></li>
                        @guest
                            <li><a href="{{ route('register') }}">Register</a></li>
                            <li><a href="{{ route('login') }}">Log In</a></li>
                        
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="{{ route('userinfor') }}" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->username }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                        <?php
                            // if(isset($_SESSION['user'])){
                            //     echo '<li><a href="">'.$_SESSION['user']['user_name'].'</a></li>';
                            //     echo '<li><a href="xuly.php?logout=1">Thoát</a></li>';
                            // }else{
                            //     echo '<li><a href="register.php">Register</a></li><li><a href="login.php">Log In</a></li>';
                            // }
                        ?>
                        
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>
            <!--End of nav-->
        </div>
        <!--End of container-->
    </div>
</div>
<!--end of header area-->