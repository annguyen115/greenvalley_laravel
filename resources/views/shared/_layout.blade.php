<!doctype html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title').GreenValley</title>
    <base href="/greenvalley_laravel/public/">
    {{-- <base href="/"> --}}
    <link rel= "shortcut icon" href="favicon.ico">
    @include('shared.layouts.style')
    @yield('custom-css')
</head>

<body>
	<!-- header -->
    @include('shared.layouts.header')
	<!-- #header -->
    {{-- content --}}
    @yield('content')
    {{-- content --}}
    {{-- footer --}}
    @include('shared.layouts.footer')
    {{-- /footer --}}
    @include('shared.layouts.script')
    @yield('custom-script')
</body>

</html>