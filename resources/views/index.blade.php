@extends('shared._layout')
@section('title','Home')
@section('custom-css')
@endsection
@section('content')
    <!-- slider -->
    @include('shared.layouts.slider')
    <!-- /slider -->
    <div class="container">
        <div id="content" class="space-top-none">
            <div class="main-content">
                <div class="space60">&nbsp;</div>
                <div class="row">
                {{-- <form class="form-horizontal" role="form" method="GET" action="{{url('advanceSearch')}}">
                        <select id="catID" style="width:200px;height:35px" name="catID">
                            <option value="">Chọn Chủ Đề</option>
                            @foreach (App\Category::all() as $cat)
                            <option value="{{$cat->id}}">{{$cat->name}}</option>
                            @endforeach
                        </select> 
                        <select id="typeID" style="width:200px;height:35px" name="typeID">
                            <option value="">Chọn Loại</option>
                            @foreach (App\Type::all() as $type)
                            <option value="{{$type->id}}">{{$type->name}}</option>
                            @endforeach
                        </select> 
                        <select id="priceID" style="width:200px;height:35px">
                            <option value="">Chọn Gia</option>
                            <option value="0-100">0-100</option>
                            <option value="100-300">100-300</option>
                            <option value="300-500">300-500</option>
                            <option value="500-1000">500-1000</option>
                            <option value="1000-2000">1000-2000</option>
                            <option value="2000-50000">2000-50000</option>
                        </select>
                        <button class="btn btn-success" type="submit" id="searchBtn">Tìm</button>
                    </form> --}}
                    <div class="col-sm-12">
                        @isset($newProduct)
                        <div class="beta-products-list">
                            <h4>New Products</h4>
                            <div class="beta-products-details">
                                {{-- <p class="pull-left">438 styles found</p> --}}
                                <div class="clearfix"></div>
                            </div>

                            <div class="row display-flex">
                                @foreach ($newProduct as $product)
                                    <div class="col-sm-3">
                                        <div class="single-item">
                                            @if ($product->sale_price>0)
                                                <div class="ribbon-wrapper">
                                                    <div class="ribbon sale">Sale</div>
                                                </div>
                                            @endif
                                            <div class="single-item-header">
                                                <a href="{{ route('ProductDetail')}}?id={{ $product->id }}"><img src="images/products/{{ $product->image }}" class="image-medium img-fluid thumbnail" alt=""></a>
                                            </div>
                                            <div class="single-item-body">
                                                <p class="single-item-title"><a href="{{ route('ProductDetail')}}?id={{ $product->id }}">{{ $product->name }}</a></p>
                                                <p class="single-item-price">
                                                    @if ($product->sale_price>0)
                                                        <span class="flash-del">${{ $product->price }}</span>
                                                        <span class="flash-sale">${{ $product->sale_price }}</span>
                                                    @else
                                                        <span>${{ $product->price }}</span>
                                                    @endif
                                                </p>
                                            </div>
                                            <div class="single-item-caption">
                                                <a class="add-to-cart pull-left" href="{{route('themgiohang',['id'=>$product->id])}}"><i class="fa fa-shopping-cart"></i></a>
                                                <a class="beta-btn primary" href="{{ route('ProductDetail')}}?id={{ $product->id }}">Details <i class="fa fa-chevron-right"></i></a>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach        
                            </div>
                        </div> <!-- .beta-products-list -->
                        @endisset

                        <div class="space50">&nbsp;</div>
                        @isset($bestSellingProduct)
                        <div class="beta-products-list">
                            <h4>Top Products</h4>
                            <div class="beta-products-details">
                                {{-- <p class="pull-left">438 styles found</p> --}}
                                <div class="clearfix"></div>
                            </div>
                            <div class="row">
                                @for ($i = 0; $i < 4; $i++)
                                    <div class="col-sm-3">
                                        <div class="single-item">
                                            @if ($bestSellingProduct[$i]->sale_price>0)
                                                <div class="ribbon-wrapper">
                                                    <div class="ribbon sale">Sale</div>
                                                </div>
                                            @endif
                                            <div class="single-item-header">
                                                <a href="{{ route('ProductDetail')}}?id={{ $bestSellingProduct[$i]->id }}"><img src="images/products/{{ $bestSellingProduct[$i]->image }}" class="image-medium img-fluid thumbnail" alt=""></a>
                                            </div>
                                            <div class="single-item-body">
                                                <p class="single-item-title"><a href="{{ route('ProductDetail')}}?id={{ $bestSellingProduct[$i]->id }}">{{ $bestSellingProduct[$i]->name }}</a></p>
                                                <p class="single-item-price">
                                                    @if ($bestSellingProduct[$i]->sale_price>0)
                                                        <span class="flash-del">${{ $bestSellingProduct[$i]->price }}</span>
                                                        <span class="flash-sale">${{ $bestSellingProduct[$i]->sale_price }}</span>
                                                    @else
                                                        <span>${{ $bestSellingProduct[$i]->price }}</span>
                                                    @endif
                                                </p>
                                            </div>
                                            <div class="single-item-caption">
                                                <a class="add-to-cart pull-left" href="{{route('themgiohang',['id'=>$product->id])}}"><i class="fa fa-shopping-cart"></i></a>
                                                <a class="beta-btn primary" href="{{ route('ProductDetail')}}?id={{ $bestSellingProduct[$i]->id }}">Details <i class="fa fa-chevron-right"></i></a>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>    
                                @endfor
                            </div>
                            <div class="space40">&nbsp;</div>
                            <div class="row">
                                @for ($i = 4; $i < 8; $i++)
                                    <div class="col-sm-3">
                                        <div class="single-item">
                                            @if ($bestSellingProduct[$i]->sale_price>0)
                                                <div class="ribbon-wrapper">
                                                    <div class="ribbon sale">Sale</div>
                                                </div>
                                            @endif
                                            <div class="single-item-header">
                                                <a href="{{ route('ProductDetail')}}?id={{ $bestSellingProduct[$i]->id }}"><img src="images/products/{{ $bestSellingProduct[$i]->image }}" class="image-medium img-fluid thumbnail" alt=""></a>
                                            </div>
                                            <div class="single-item-body">
                                                <p class="single-item-title"><a href="{{ route('ProductDetail')}}?id={{ $bestSellingProduct[$i]->id }}">{{ $bestSellingProduct[$i]->name }}</a></p>
                                                <p class="single-item-price">
                                                    @if ($bestSellingProduct[$i]->sale_price>0)
                                                        <span class="flash-del">${{ $bestSellingProduct[$i]->price }}</span>
                                                        <span class="flash-sale">${{ $bestSellingProduct[$i]->sale_price }}</span>
                                                    @else
                                                        <span>${{ $bestSellingProduct[$i]->price }}</span>
                                                    @endif
                                                </p>
                                            </div>
                                            <div class="single-item-caption">
                                                <a class="add-to-cart pull-left" href="{{route('themgiohang',['id'=>$product->id])}}"><i class="fa fa-shopping-cart"></i></a>
                                                <a class="beta-btn primary" href="{{ route('ProductDetail')}}?id={{ $bestSellingProduct[$i]->id }}">Details <i class="fa fa-chevron-right"></i></a>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>    
                                @endfor
                            </div>
                        </div> <!-- .beta-products-list -->
                        @endisset
                    </div>
                </div> <!-- end section with sidebar and main content -->


            </div> <!-- .main-content -->
        </div> <!-- #content -->
    </div>
@endsection
@section('custom-script')
@endsection

