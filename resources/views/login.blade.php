@extends('shared._layout')
@section('title','Login')
@section('custom-css')
    <link href="build/css/LogIn.css" rel="stylesheet" id="bootstrap-css">
@endsection
@section('content')
<div style="background-image: url('images/nen4.jpg');padding:60px;min-height: 610px;">
    <div class="container">
        <div class="card card-container">
            <h2 style="text-align: center; font-weight: bold" >Log In</h2>
            <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
            <!-- <img id="profile-img" class="profile-img-card" src="img/avatar.png" /> -->
            <p id="profile-name" class="profile-name-card"></p>
            <form class="form-signin" action="xuly.php" method="POST">
                <?php
                    if(isset($_SESSION['error']['login']) && $_SESSION['error']['login'] == 1){
                        unset($_SESSION['error']['login']); 
                        echo '<span class="error_message" style="color:red;margin-bottom:5px;">Email or password invalid</span>';
                    }
                ?>   
                <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" required autofocus>
                <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
                <div id="remember" class="checkbox">
                    <label>
                        <input type="checkbox" value="remember-me"> Remember me
                        <a href="#" class="forgot-password" style="margin-left:10px;">
                                Forgot the password?
                            </a>
                    </label>
                </div>
                <button class="btn btn-lg btn-primary btn-block btn-signin" name="login" type="submit">Sign in</button>
            </form><!-- /form -->
            <!-- Social login -->
            <p style="margin-left: 90px;">or sign in with:</p>
            <div class="icon">
                <a type="button" class="btn-floating btn-fb btn-sm " >
                    <i class="fa fa-facebook"></i>
                </a>
                <a type="button" class="btn-floating btn-tw btn-sm ">
                    <i class="fa fa-twitter"></i>
                </a>
                <a type="button" class="btn-floating btn-li btn-sm  ">
                    <i class="fa fa-linkedin"></i>
                </a>
                <a type="button" class="btn-floating btn-git btn-sm ">
                    <i class="fa fa-github"></i>
                </a>
            </div>
        </div><!-- /card-container -->
    </div><!-- /container -->
</div>
@endsection
@section('custom-script')
    <script src="build/js/LogIn.js"></script>
@endsection