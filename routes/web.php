<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// home
// Route::view('{name}','home')->where(['name' => '|home|index']);
// Route::group([ 'prefix' => 'user' ] , function(){
//     Route::get('login','AuthenController@getLogin');
//     Route::post('login','AuthenController@postLogin');
//     Route::get('register', 'AuthenController@getRegister');

// });
Auth::routes();
Route::get('{name}', 'HomeController@getHome')->where(['name' => '|home|index'])->name('home');
Route::get('about', 'HomeController@getAbout')->name('about');
Route::get('contact', 'HomeController@getContact')->name('contact');
// Route::view('shopping_cart', 'shopping_cart');
Route::group([ 'prefix' => 'product' ] , function(){
    Route::get('detail','ProductController@getDetail')->name('ProductDetail');
    Route::get('listbycategory', 'ProductController@getListByCategory')->name('listbycategory');
    Route::get('listbytype', 'ProductController@getListByType')->name('listbytype');
    Route::get('listall', 'ProductController@getListAll')->name('listall');
    Route::get('sortbytype', 'ProductController@getSortByType')->name('sortbytype');
    Route::get('search', 'ProductController@getSearch')->name('search');
});
Route::group([ 'prefix' => 'user' ] , function(){
    Route::get('infor','UserController@getInfor')->name('userinfor');
    Route::post('update','UserController@updateInfor')->name('updateInfor');
});
Route::get('test', function () {
    // $product = App\Category::find(5)->Product->Where('type_id',1)->toArray();
    // return var_dump($product);
    $comment = App\Comment::with('User')->get()->toArray();
    
    return var_dump($comment);
});

//advance Search
Route::get('advanceSearch',function(){
    return View('advanceSearch',['data'=> App\Product::all(),'catByUser' => '']);
});
Route::get('advanceSearch/{type}','ProductController@proType');
Route::get('advanceSearch/{cate}','ProductController@proCategories');
// ajax
Route::get('sortSearch', 'ProductController@sortSearch');
//gio hang
Route::get('add-to-cart/{id}',[
    'as'=> 'themgiohang',
    'uses'=>'ProductController@getAddtoCart'
    ]);
//Show gio hang
Route::get('shoppingCart','ProductController@getCart')->name('shoppingCart');
// Route::get('shoppingCart','ProductController@getCart')->name('shoppingCart');

Route::get('del-cart/{id}',[
	'as'=>'xoagiohang',
	'uses'=>'ProductController@getDelItemCart'
]);
Route::get('dat-hang',[
	'as'=>'dathang',
	'uses'=>'ProductController@getCheckout'
]);

Route::post('dat-hang',[
	'as'=>'dathang',
	'uses'=>'ProductController@postCheckout'
]);
Route::get('session/destroy', function (Illuminate\Http\Request $request) {
    $request->session()->flush();
    return redirect()->back();
});
Route::get('comment/getCommentByProduct/{id}','CommentController@getComment')->name('getComments');
Route::post('comment/insertComment','CommentController@InsertComment')->name('insertComment');
Route::get('a', function () {
    return \Request::get('type');
})->middleware('eve');