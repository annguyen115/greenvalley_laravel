<?php

use Illuminate\Database\Seeder;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function emptyTable(){
        //turn off check foreign key
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        //list table
        $tableNames = Schema::getConnection()->getDoctrineSchemaManager()->listTableNames();
        foreach ($tableNames as $name) {
            //if you don't want to truncate migrations
            if ($name == 'migrations') {
                continue;
            }
            DB::table($name)->truncate();
        }
        //turn on check foreign
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
    public function run()
    {
        //empty all table
        $this->emptyTable();
        //insert data to database
        $this->call([
            UserSeeder::class,
            CategorySeeder::class,
            TypeSeeder::class,
            ProductSeeder::class,
            CommentSeeder::class,
            OrderSeeder::class,
            OrderDetailSeeder::class,
            CategoryProductSeeder::class,
            ]);
    
    }
}
