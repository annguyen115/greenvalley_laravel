<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [ 
            [ 'name'=>'Sinh Nhật', 'description'=>'Hoa tặng sinh nhật' ], 
            [ 'name'=>'Hoa Mừng 8/3', 'description'=>'Hoa mừng 8/3 quốc tế phụ nữ' ], 
            [ 'name'=>'Sinh Nhật', 'description'=>'Hoa tặng sinh nhật' ], 
            [ 'name'=>'Hoa Khai Trương', 'description'=>'' ], 
            [ 'name'=>'Hoa Cưới', 'description'=>'' ], 
            [ 'name'=>'Hoa Ngày Lễ', 'description'=>'' ], 
            [ 'name'=>'Hoa Chia Buồn', 'description'=>'' ], 
        ];
        DB::table('categories')->insert($categories);
    }
}
