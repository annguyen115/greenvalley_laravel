<?php

use Illuminate\Database\Seeder;

class OrderDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $order_detail = [ 
            [ 'order_id'=>1, 'product_id'=>1, 'quantity'=>3, 'price'=>100, ], 
            [ 'order_id'=>1, 'product_id'=>2, 'quantity'=>5, 'price'=>126, ], 
            [ 'order_id'=>2, 'product_id'=>1, 'quantity'=>5, 'price'=>60, ], 
            [ 'order_id'=>2, 'product_id'=>3, 'quantity'=>5, 'price'=>85.95, ] 
        ];
        DB::table('order_detail')->insert($order_detail);
    }
}
