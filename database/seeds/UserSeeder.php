<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [ 
            [ 'username'=>'many', 'password'=>bcrypt('123456'), 'email'=>'many@localhost.com', 'phone'=>'0931437267', 'address'=>'many localhost', 'dob'=>'1997-09-20', 'randomkey'=>'123456', 'active'=>'actived'], 
            [ 'username'=>'admin', 'password'=>bcrypt('123456'), 'email'=>'admin@localhost.com', 'phone'=>'0931437267', 'address'=>'admin localhost', 'dob'=>'1997-09-20', 'randomkey'=>'123456', 'active'=>'actived'], 
            [ 'username'=>'eve', 'password'=>bcrypt('123456'), 'email'=>'eve@localhost.com', 'phone'=>'0931437267', 'address'=>'eve localhost', 'dob'=>'1997-09-16', 'randomkey'=>'123456', 'active'=>'actived'], 
            [ 'username'=>'boxuongnho', 'password'=>bcrypt('123456'), 'email'=>'bemine4ever@localhost.com', 'phone'=>'0931437267', 'address'=>'123 Đào trí', 'dob'=>'1997-09-16', 'randomkey'=>'123456', 'active'=>'not_active'] 
        ];
        DB::table('users')->insert($users);
    }
}
