<?php

use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $comments = [ 
                        [ 'user_id'=>1, 'product_id'=>1, 'content'=>'Hoa Hồng Đẹp' ], 
                        [ 'user_id'=>1, 'product_id'=>1, 'content'=>'ừ đẹp thật' ], 
                        [ 'user_id'=>2, 'product_id'=>2, 'content'=>'Mua tặng bạn gái thôi mọi người' ], 
                        [ 'user_id'=>3, 'product_id'=>3, 'content'=>'Hoa dởm mọi người ơi :))' ] 
                    ];
        DB::table('comments')->insert($comments);
    }
}
