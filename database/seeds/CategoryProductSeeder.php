<?php

use Illuminate\Database\Seeder;

class CategoryProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category_product = [ 
            [ 'category_id'=>5, 'product_id'=>1 ], 
            [ 'category_id'=>5, 'product_id'=>2 ], 
            [ 'category_id'=>5, 'product_id'=>3 ], 
            [ 'category_id'=>2, 'product_id'=>1 ],
            [ 'category_id'=>1, 'product_id'=>6 ], 
            [ 'category_id'=>1, 'product_id'=>7 ],
            [ 'category_id'=>1, 'product_id'=>26 ],
            [ 'category_id'=>1, 'product_id'=>28 ],
            [ 'category_id'=>1, 'product_id'=>29 ],
            [ 'category_id'=>1, 'product_id'=>30 ], 
            [ 'category_id'=>2, 'product_id'=>29 ],
            [ 'category_id'=>2, 'product_id'=>30 ], 
            [ 'category_id'=>2, 'product_id'=>23 ], 
            [ 'category_id'=>2, 'product_id'=>24 ], 
            [ 'category_id'=>2, 'product_id'=>26 ], 
            [ 'category_id'=>3, 'product_id'=>21 ],
            [ 'category_id'=>3, 'product_id'=>23 ], 
            [ 'category_id'=>3, 'product_id'=>22 ], 
            [ 'category_id'=>3, 'product_id'=>25 ], 
            [ 'category_id'=>3, 'product_id'=>6 ], 
            [ 'category_id'=>3, 'product_id'=>7 ], 
            [ 'category_id'=>4, 'product_id'=>23 ], 
            [ 'category_id'=>4, 'product_id'=>21 ], 
            [ 'category_id'=>4, 'product_id'=>25 ], 
            [ 'category_id'=>4, 'product_id'=>10 ], 
            [ 'category_id'=>4, 'product_id'=>11 ],
        ];
        DB::table('category_product')->insert($category_product);//
    }
}
