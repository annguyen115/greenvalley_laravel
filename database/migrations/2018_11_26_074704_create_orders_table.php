<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('receiver',50)->comment('Tên người nhận hàng');
            $table->string('address',100)->comment('Địa chỉ người nhận hàng');
            $table->string('email',50);
            $table->string('phone',20);
            $table->text('note')->nullable();
            $table->enum('status', ['delivery', 'delivered'])->default('delivery')->comment('Trạng thái gói hàng');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
