<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username',20);
            $table->string('password');
            $table->string('email',50);
            $table->string('phone',12);
            $table->string('address',50);
            $table->date('dob',50);
            $table->string('randomkey')->nullable();
            $table->string('remember_token', 100)->nullable();
            $table->tinyInteger('isAdmin')->default(0);
            $table->enum('active', ['actived', 'not_active'])->default('actived');
            $table->unique('email');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            // $table->timestamps()->default(DB::raw('CURRENT_TIMESTAMP'));;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
