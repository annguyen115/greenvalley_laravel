<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type_id')->unsigned();
            $table->string('name',50);
            $table->decimal('price', 14, 2);
            $table->decimal('sale_price', 14, 2)->default(-1)->comment('giá sale');;
            $table->integer('stock')->comment('Số lượng tồn kho');
            $table->text('description')->nullable();
            $table->string('image')->comment('Ảnh');
            $table->integer('purchase')->default(0)->comment('Số lượt mua');
            $table->integer('view')->default(0)->comment('Số lượt xem');
            $table->string('note')->nullable()->comment('Ghi chú');
            $table->enum('visibility', ['show', 'hidden'])->default('show')->comment('Ẩn hiện sản phẩm');;
            $table->foreign('type_id')->references('id')->on('types');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
