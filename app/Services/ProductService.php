<?php
namespace App\Services;
use DB;
use App\Product;
use App\Category;
// use Exception;
class ProductService
{
    static protected $productLimit = 9;
    public static function getAllProduct($page){
        //pagging
        //get position
        $position = ($page - 1) * self::$productLimit;
        $products = Product::Where('visibility','show')->orderBy('id','DESC')->skip($position)->take(self::$productLimit);
        return $products->get();
    }
    
    public static function getNewProduct($limit=0){
        $products = Product::Where('visibility','show');
        if($limit>0)
            $products = $products->take($limit);
        return $products->orderBy('created_at','DESC')->get();
    }
    public static function getBestSellProduct($limit=0){
        $products = Product::Where('visibility','show');
        if($limit>0)
            $products = $products->take($limit);
        return $products->orderBy('view','DESC')->get();;
    }
    public static function getProductDetail($id){
        $products = Product::Where('id',$id)->Where('visibility','show');
        $count = $products->count();
        if (!$count) abort(404);
        return $products->first();
    }
    public static function findProduct($key){
        $products = Product::Where('name','like','%'.$key.'%')->Where('visibility','show');
        return $products->get();
    }

    public static function countProductByCategory($category_id,$type_id){
        $products = Category::find($category_id)->Product->Where('visibility','show');
        if($type_id!=-1)
            $products = $products->Where('type_id',$type_id);
        return $products->count();
    }
    public static function countProductByType($type_id){
        $products = Product::Where('type_id',$type_id)->Where('visibility','show');
        return $products->count();
    }
    public static function countProduct(){
        $products = Product::Where('visibility','show');
        return $products->count();
    }
    public static function getProductByCategory($category_id,$type_id,$position){
        $products = Category::find($category_id)->Product->Where('visibility','show')->sortByDesc('created_at');
        if($type_id!=-1)
            $products = $products->Where('type_id',$type_id);
        return $products->slice($position)->take(self::$productLimit)->all();
    }
    public static function getProductByCategoryAndType($category_id,$type_id){
        if($category_id!=-1)
            $products = Category::find($category_id)->Product->Where('visibility','show')->sortByDesc('created_at');
        else
            $products = Product::Where('visibility','show')->orderBy('created_at','DESC');
        if($type_id!=-1)
            $products = $products->Where('type_id',$type_id);
        return $category_id!=-1 ? $products->all() : $products->get();
    }
    public static function getProductByType($type_id,$position){
        $products = Product::Where('type_id',$type_id)->Where('visibility','show')->orderBy('created_at','DESC');
        return $products->skip($position)->take(self::$productLimit)->get();
    }
    public static function getCategoryFromProduct($id){
        $category = Product::find($id)->Category;
        return $category->all();
    }
    public static function getProductRelate($product,$category){
        $price = $product->price;
        $min = $price - ($price*30/100);
        $max = $price + ($price*30/100);
        $categoryArray = [];
        foreach ($category as $a) {
            array_push($categoryArray,$a->id);
        }
        $categoryArray = $categoryArray[0];
        // $categoryArray = implode(',',$categoryArray);
        $relatedProducts = Product::Where('id','!=',$product->id)->Where('visibility','show')->WhereBetween('price',[$min,$max]);
        // $relatedProducts->whereHas('Category',function($query){
        //     // $query->Where('category_id',$categoryArray[0]);
        // });
        return $relatedProducts->limit(3)->get();
        // return $categoryArray[0];
        // return [$min,$max];
    }
}