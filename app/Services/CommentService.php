<?php
namespace App\Services;
use App\Comment;
use App\User;
// use Exception;
class CommentService
{
    public static function getComentByIdProduct($id){
        $comment = Comment::with('User')->Where('product_id',1)->Where('visibility','show')->orderBy('created_at','DESC');
        return $comment->get();
    }
    public static function countComentByIdProduct($id){
        $comment = Comment::with('User')->Where('product_id',1)->Where('visibility','show');
        return $comment->count();
    }
   
}