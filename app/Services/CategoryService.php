<?php
namespace App\Services;
use App\Category;
// use Exception;
class CategoryService
{
    public static function getAllCategory($limit=0){
        $categories = Category::get();
        if($limit>0)
            $categories = $categories->take($limit);
        return $categories;
    }
    public static function findCategory($id){
        $categories = Category::Where('id',$id);
        $count = $categories->count();
        if (!$count) abort(404);
        return $categories->first();
    }
    
}