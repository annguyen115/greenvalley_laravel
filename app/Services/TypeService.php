<?php
namespace App\Services;
use App\Type;
// use Exception;
class TypeService
{
    public static function getAllType($limit=0){
        
        $types = Type::get();
        if($limit>0)
            $types = $types->take($limit);
        return $types;
    }
    public static function findType($id){
        $types = Type::Where('id',$id);
        $count = $types->count();
        if (!$count) abort(404);
        return $types->first();
    }
}