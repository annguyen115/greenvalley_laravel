<?php
namespace App\Helpers;
use App\Type;
class TypeHelper
{
    public static function getType(){
        $types = Type::get();
        return $types;
    }
}