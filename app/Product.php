<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Product extends Model
{
    protected $table = 'products';
    public function Type(){
        return $this->belongsTo('App\Type','type_id','id');
    }
    public function Category(){
        return $this->belongsToMany(Category::class, 'category_product', 'product_id','category_id');
    }
}
