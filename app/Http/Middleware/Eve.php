<?php

namespace App\Http\Middleware;

use Closure;

class Eve
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $type = \EveType::getType();
        $request->attributes->add(['type'=>$type]);
        return $next($request);
    }
}
