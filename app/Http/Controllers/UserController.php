<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getInfor(){
        $id = Auth::user()->id;
        $user = User::find($id)->first();
        // return var_dump($user);
        return View('userinfor',['user'=>$user]);
    }
    public function updateInfor(Request $request){
        $id = Auth::user()->id;
        $user = User::find($id)->first();
        $user->username = $request->username;
        if($request->changePassword){
            if($request->password == $request->confirmPass){
                $user->password = bcrypt($request->password);
                
            }else{
                return redirect()->back()->withInput()->withErrors(["error"=>"Comfirm passwork wrong"]);;
            }
        }
        $user->save();
        return redirect()->back();
    }
}
