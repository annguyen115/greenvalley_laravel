<?php

namespace App\Http\Controllers;
use App\Services\CommentService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Comment;
use Carbon\Carbon;

class CommentController extends Controller
{
    public function getComment($id){
        $count = CommentService::countComentByIdProduct($id);
        $comments = CommentService::getComentByIdProduct($id);
        $view =  view('ajax.comment',['comments'=>$comments])->render();
        $view = trim(preg_replace('/\r\n/', ' ', $view));
        // $returnHTML = view('comment')->with('comments', $comments)->render();
        return response()->json(['view'=>$view,'count'=>$count],200,['Content-type'=> 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
    }
    public function InsertComment(Request $request){
        $currentTime = Carbon::now('Asia/Ho_Chi_Minh');
        if ($request->comment!=null) {
            $comment = new Comment;
            $comment->product_id = $request->pid;
            $comment->user_id = Auth::user()->id; 

            $comment->content = $request->comment;
            $comment->created_at = $currentTime->toDateTimeString();
            $comment->updated_at = $currentTime->toDateTimeString();
            $comment->save();
            return redirect()->back();
        }else{

            return redirect()->back()->withErrors(["error"=>"This field is required"]);
        } 
    }
}
