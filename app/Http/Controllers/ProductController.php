<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ProductService;
use App\Services\CategoryService;
use App\Services\TypeService;
use App\Type;
use App\Product;
use App\Cart;
use App\User;
use App\Order;
use App\OrderDetail;
use DB;
use Session;
use Auth;
if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
    // Ignores notices and reports all other kinds... and warnings
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
    // error_reporting(E_ALL ^ E_WARNING); // Maybe this is enough
}
class ProductController extends Controller
{   
    public static $productLimit = 9;
    public function getProductList(){
        $data = ProductService::getAllProduct();
        return var_dump($data);
        // return view('productlist',['productlist'=>$data]);
    }



    public function getNewProduct(){
        $products = ProductService::getNewProduct(5);
        return view('test',['data'=>$products]);
    }



    public function getDetail(Request $request){
        if (!$request->has('id')) {
            abort(404);
        }
        $id = $request->id;
        // get 4 newest products`
        $newProduct = ProductService::getNewProduct(4);
        //get 4 best selling products
        $bestSellingProduct = ProductService::getBestSellProduct(4);
        
        //get product detail
        $product = ProductService::getProductDetail($id);
        //get category of product
        $categories = ProductService::getCategoryFromProduct($id);
        $relateProduct = ProductService::getProductRelate($product,$categories);
        //for menu
        $categories = CategoryService::getAllCategory();
        return view('detail',['product'=>$product,'newProduct'=>$newProduct,'bestSellingProduct'=>$bestSellingProduct,'categories' => $categories,'relateProducts' =>$relateProduct,'listCategory'=>$categories]);
        // return var_dump($relateProduct);
    }



    public function getListByCategory(Request $request){
        if ((!$request->has('category_id'))&&(empty($request->category_id))) {
            abort(404);
        }
        //get param in route
        $category_id= $request->has('category_id') ? $request->category_id:-1;
        $type_id= $request->has('type_id')&& !empty($request->type_id) ? $request->type_id:-1;
       
        $page = $request->has('page')&& !empty($request->page) ? $request->page:1;
        $position = ($page - 1) * self::$productLimit;

        //get Category List
        $categoryInfo = CategoryService::findCategory($category_id);
        $categories = CategoryService::getAllCategory();
        //get Type list
        $types = TypeService::getAllType();

        //get number of product by category
        $productTotal = ProductService::countProductByCategory($category_id,$type_id);
        $pageTotal = ceil($productTotal/self::$productLimit);
        //get Product by category
        $products = ProductService::getProductByCategory($category_id,$type_id,$position);
        // return var_dump($products);
        return view('product_listby_cate',['category' => $categoryInfo,'listCategory'=>$categories, 'listType'=>$types, 'listProduct'=>$products, 'pageTotal'=>$pageTotal]);
    }
    public function getListByType(Request $request){
        if ((!$request->has('type_id'))&&(empty($request->type_id))) {
            abort(404);
        }
        //get param in route
        $type_id= $request->has('type_id') ? $request->type_id:-1;
        $page = $request->has('page')&& !empty($request->page) ? $request->page:1;
        $position = ($page - 1) * self::$productLimit;

        //get Category List
        $typeInfo = TypeService::findType($type_id);
        $categories = CategoryService::getAllCategory();
        //get Type list
        $types = TypeService::getAllType();
        
        //get number of product by category
        $productTotal = ProductService::countProductByType($type_id);
        $pageTotal = ceil($productTotal/self::$productLimit);
        //get Product by category
        $products = ProductService::getProductByType($type_id,$position);
        return view('product_listby_type',['type' => $typeInfo,'listCategory'=>$categories, 'listType'=>$types, 'listProduct'=>$products, 'pageTotal'=>$pageTotal]);
    }



    public function getSearch(Request $request){
        if (!$request->has('key')) {
            abort(404);
        }
        $key = $request->key;
        //find
        $products = ProductService::findProduct($key);
        return view('search',['productList'=>$products]);
    }


    public function getListAll(Request $request){
        //param route
        $page = $request->has('page')&& !empty($request->page) ? $request->page:1;
        
        //get product list
        $products = ProductService::getAllProduct($page);
        //count product
        $productTotal = ProductService::countProduct();
        $pageTotal = ceil($productTotal/self::$productLimit);


        //get Category List
        $categories = CategoryService::getAllCategory();
        //get Type list
        $types = TypeService::getAllType();

        return view('product_listall',['listCategory'=>$categories, 'listType'=>$types, 'listProduct'=>$products, 'pageTotal'=>$pageTotal]);
        // return view('productlist',['productlist'=>$data]);
    }

    //advance search
    public function proType(Request $request){
        $type= $request->type;
        //start query for search
        $data= DB::table('types')
        ->join('products','types.id','=','products.type_id')
        ->where('types.name',$type)
        ->get();
        return view('advanceSearch',[
          'data' => $data , 'catByUser' => $type
        ]);
     }
     public function proCategories(Request $request){// lay product theo category
         $categories= $request->categories;
         //start query for search
         $data= DB::table('categories')
         ->join('category_product','categories.id','=','category_product.category_id')
         ->join('products','category_product.product_id','=','products.id')
         ->where('catgories.name',$categories)
         ->get();
         return view('advanceSearch',[
           'data' => $data , 'catByUser' => $categories
         ]);
      }

       //  select p.name, c.name 
    //  from categories c
    //  inner JOIN category_product cp
    //  on c.id = cp.category_id
    //  inner JOIN products p
    //  on cp.product_id = p.id
    //  group by c.name

//ajax Search
    public function sortSearch(Request $request){
        $type_id = $request->type_id;
        $count =@count($request->price);
        
        //phai check cho nay xem price co ton tai hong ne
        // if($request->has('price')){
            if($type_id!="" && $count!="0"){
                $price= explode("-",$request->price);
               
                $start= $price[0];
                $end= $price[1];

                $data = DB::table('types')
                ->join('products','products.type_id','=','types.id')
                ->where('products.type_id',$type_id)
                ->where('products.price',">=",$start)
                ->where('products.price',"<=",$end)
                ->get();
               // echo "both are selected";
            }else if($count!="0"){
                $price= explode("-",$request->price);
               
                $start= $price[0];
                $end= $price[1];

                $data = DB::table('products')
                ->join('types','types.id','=','products.type_id')
                ->where('products.price',">=",$start)
                ->where('products.price',"<=",$end)
                ->get();
               // echo "price is selected";
            } else if($type_id!=""){
                $data = DB::table('types')
                ->join('products','products.type_id','=','types.id')
                ->where('products.type_id',$type_id)
                ->get();
               // echo "cat is select";
            }else {
                echo "<h1 align='center'>Chọn ít nhất 1 trường để tìm kiếm hoa</h1>";
              //  echo "nothing is select";
            }
            if(count($data)=="0"){
                echo "<h1 align='center'>no products found under this Category".$start."-".$end."range</h1>";
            }else{
                return view('advanceSearch',['data'=>$data,'catByUser'=> $data[0]->name]);
    
            }
    }



    //thêm giỏ hàng
    public function getAddtoCart(Request $request ,$id){

        $product = Product::find($id);
        $oldCart = Session('cart')?Session::get('cart'):null;//oldCart dung o Cart Model
       // $oldCart= Session::has('cart') ? Session::get('cart'):null;
        $cart = new Cart($oldCart);
        $cart->add($product, $product->id);
        
        $request->session()->put('cart',$cart);
        return redirect()->back();

    }
//show page gio hang
    public function getCart(){
        if( !Session::has('cart')) {
            return view('shoppingCart');
        }
        $oldCart =Session::get('cart');
        $cart= new Cart($oldCart);
       
      // dd($cart);die;
        // var_dump();
        return view('shoppingCart',['products'=> $cart, 'totalPrice'=>$cart->toltalPrice,'product_cart'=>$cart->$items->price]);
    }
   
    //xóa giỏ hàng
    public function getDelItemCart($id){
        $oldCart = Session::has('cart')?Session::get('cart'):null;
        $cart = new Cart($oldCart);
        $cart->removeItem($id);
        if(count($cart->items)>0){
            Session::put('cart',$cart);
        }
        else{
            Session::forget('cart');
        }
        return redirect()->back();
    }
    //đặt hàng
    public function getCheckout(){
      //  return view('dat_hang');
      if(Session('cart')){
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        return view('dat_hang',[ 'product_cart'=>$cart->items,'totalPrice'=>$cart->totalPrice,'totalQty'=>$cart->totalQty]);
    }
    }

    public function postCheckout(Request $req){
        $cart = Session::get('cart');
        $bill = new Order;
        $bill->user_id = Auth::user()->id;
        $bill->receiver= Auth::user()->username;
        $bill->created_at = date('Y-m-d');
        $bill->updated_at= date('Y-m-d');
        $bill->address= Auth::user()->address;
        
        // $comment->created_at = $currentTime->toDateTimeString();
        // $comment->updated_at = $currentTime->toDateTimeString();
       // $bill->total = $cart->totalPrice;
        //$bill->payment = $req->payment_method;
      //  $bill->note = $req->notes;
        $bill->save();

        foreach ($cart->items as $key => $value) {
            $bill_detail = new OrderDetail;
            $bill_detail->order_id = $bill->id;
            $bill_detail->product_id = $key;//$value['item']['value']
            $bill_detail->quantity = $value['qty'];
            $bill_detail->price = ($value['price']/$value['qty']);
            $bill_detail->save();
        }
        Session::forget('cart');
        return redirect()->back();
        //->back()->with('thongbao','Đặt hàng thành công');

    }
    public static function getSortByType(Request $request){
        $category_id = $request->has('category_id')&& !empty($request->category_id) ? $request->category_id:-1;
        $type_id = $request->type_id;
        $products = ProductService::getProductByCategoryAndType($category_id,$type_id);
        $view =  view('ajax.product-list',['listProduct'=>$products])->render();
        $view = trim(preg_replace('/\r\n/', ' ', $view));
        return response()->json(['view'=>$view],200,['Content-type'=> 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
    }
}
