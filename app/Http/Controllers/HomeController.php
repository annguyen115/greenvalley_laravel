<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ProductService;
use App\Services\CategoryService;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getHome()
    {
        //get 4 new product
        $newProduct = ProductService::getNewProduct(4);
        // return var_dump($newProduct);
        // get 8 best selling product
        $bestSellingProduct = ProductService::getBestSellProduct(8);
        // $types = \EveType::getType();
        $categories = CategoryService::getAllCategory();
        return view('index',['newProduct'=>$newProduct,'bestSellingProduct'=>$bestSellingProduct,'listCategory'=>$categories]);
    }
    public function getAbout()
    {
        $categories = CategoryService::getAllCategory();
        return view('about',['listCategory'=>$categories]);
    }
    public function getContact()
    {
        $categories = CategoryService::getAllCategory();
        return view('contact',['listCategory'=>$categories]);
    }
}
